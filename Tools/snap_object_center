def xformAverage(drivers):
    """
    This function returns the averaged trans and rot of a selection.
    
    """
    
    drivers_trans = []
    drivers_rot = []
    
    # gather all the trans and rot values
    for obj in drivers:
        
        # get the translation values
        obj_trans = cmds.xform(obj, query=True, translation=True, worldSpace=True)
        drivers_trans.append(obj_trans)
        
        # get the rotation values    
        obj_rot = cmds.xform(obj, query=True, rotation=True, worldSpace=True)
        drivers_rot.append(obj_rot)
        
    # sort selection position list into their x, y, z lists    
    obj_trans_xyz = zip(*drivers_trans)
    
    trans_x = sum(obj_trans_xyz[0]) / len(obj_trans_xyz[0])
    trans_y = sum(obj_trans_xyz[1]) / len(obj_trans_xyz[1])
    trans_z = sum(obj_trans_xyz[2]) / len(obj_trans_xyz[2])
    
    trans = (trans_x, trans_y, trans_z)       
        
    # sort selection rotation list into their x, y, z lists    
    obj_rot_xyz = zip(*drivers_rot)
    
    rot_x = sum(obj_rot_xyz[0]) / len(obj_rot_xyz[0])
    rot_y = sum(obj_rot_xyz[1]) / len(obj_rot_xyz[1])
    rot_z = sum(obj_rot_xyz[2]) / len(obj_rot_xyz[2])
    
    rot = (rot_x, rot_y, rot_z)
    
    return (trans,rot)
        

# Get the selection list
object_list = cmds.ls(sl=True)

# List slicing, last selected be the driven object
driver_list = object_list[0:len(object_list)-1]
driven_object = object_list[-1]

obj_average = xformAverage(driver_list)

# Move the driven object to the correct spot
cmds.xform(driven_object, translation=obj_average[0])
cmds.xform(driven_object, rotation=obj_average[1])